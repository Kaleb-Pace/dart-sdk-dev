FROM archlinux:base-devel

# Insatll dependencies
RUN pacman -Syy --noconfirm \
    git \
    jq \
    openssh \
    unzip

# Enable nobody user and give sudo permissions to build pkg
RUN sed -i -e "s/\(nobody.*\)\/sbin\/nologin/\1\/bin\/bash/" /etc/passwd \
    && echo "%nobody ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Give nobody permissions to build dirs and ssh key access to publish to aur
RUN mkdir -p /home/nobody/.ssh /tmp/nobody \
    && chown nobody:nobody -R /home/nobody /tmp/nobody \
    && chmod 700 /home/nobody/.ssh

# Configure makepkg environment
ENV BUILDDIR=/tmp/nobody \
    PKGDEST=/tmp/nobody \
    SRCDEST=/tmp/nobody
