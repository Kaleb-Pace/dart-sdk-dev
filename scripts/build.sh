#!/bin/bash

export VERSION=$(curl -s "https://storage.googleapis.com/dart-archive/channels/dev/release/latest/VERSION" | jq '.version' | sed -e 's/-/_/' -e 's/\"//g' )
export SHA256SUM=$(curl -s "https://storage.googleapis.com/dart-archive/channels/dev/release/${VERSION//_/-}/sdk/dartsdk-linux-x64-release.zip.sha256sum" | cut -f1 -d" " )
sed -e "s/VERSION_PLACEHOLDER/$VERSION/" -e "s/SHASUM_PLACEHOLDER/$SHA256SUM/" ./PKGBUILD_TEMPLATE > ./PKGBUILD