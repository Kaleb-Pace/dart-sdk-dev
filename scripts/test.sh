#!/bin/bash

sed -i -e "s/PKGEXT.*/PKGEXT='\.pkg\.tar'/" /etc/makepkg.conf
su - nobody -s /bin/bash -c "cd ${CI_PROJECT_DIR} && makepkg -s --noconfirm --install"
if [ -e /opt/dart-sdk-dev/bin/dart ] && [ -e /opt/dart-sdk-dev/bin/pub ]; then echo 0; else echo 1; fi
