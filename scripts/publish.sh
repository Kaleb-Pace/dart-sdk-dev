#!/bin/bash

export VERSION=$(curl -s "https://storage.googleapis.com/dart-archive/channels/dev/release/latest/VERSION" | \
                    jq '.version' | sed -e 's/-/_/' -e 's/\"//g' )

git config --global user.email "kaleb.pace@pm.me"
git config --global user.name "Kaleb Pace"
eval $(ssh-agent -s)
echo "$SSH_KEY" | tr -d '\r' | ssh-add - > /dev/null
export GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no'
git clone ssh://aur@aur.archlinux.org/dart-sdk-dev.git
su - nobody -s /bin/bash -c "cd ${CI_PROJECT_DIR} && makepkg --printsrcinfo > .SRCINFO"
cd dart-sdk-dev 
cp ../PKGBUILD ../.SRCINFO .
git add .
git commit -m "Update to ${VERSION}"
git push